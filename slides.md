---
title: git guide
theme: night
---
# git guide

`Author: Max Foo <max.foo@credit-suisse.com>`

---

## Using git as a team

----

## A set of conventions

- All changes eventually merge to a single branch (usually `master`)
- Don't break `master` branch
- Keep public history (`master`) **clean**
  - Group commits into merge requests (MRs)
  - Squash unnecessary commits when merging into `master`

----

### Using these conventions

- Alice and Bob are working on a repository
- Both creates their branches from `master` to work on feature A and B
- Both commits some changes to their branches, pushes and creates 2 merge requests (MR)
- Some tests may fail on the MRs' pipelines and they make new commits to fix them
- They review each others' MR, and approve it
- Both MRs are then merged to `master`

---

## git commands

----

### To work on a repository

**Remote**

1. Create/view a repository in Gitlab/Github etc.

```text
git clone <url>
```

**Local**

```text
git init
git remote add <name> <url>
```

----

### Pulling the latest commits

```text
git pull
```

or if you have commits not pushed

```text
git pull -r
```

----

### To work on a branch

**Existing branch**

```text
git checkout <branch-name>
```

**New branch from master**

```text
git checkout master
git pull
git checkout -b <new-branch-name>
```

----

### View commit

**Last commit**

```text
git show
```

**Specific commit**

```text
git show <commitid>
```

----

### View history

**Current branch**

```text
git log
```

**Specific branch**

```text
git log <branch-name>
```

----

### Staging/Unstaging changes

**Stage all changes**

```text
git add -A
```

**Stage a file**

```text
git add <file>
```

**Unstage all changes**

```text
git reset
```

----

### Discarding changes

**Discard changes for a file**

```text
git checkout -- <file>
```

**Discard all changes**

```text
git reset --hard
```

----

### Stashing

Stash all current changes

```text
git stash
```

Apply changes in stash

```text
git stash pop
```

----

### Commiting changes

**New commit**

```text
git commit -m <commit-msg>
```

**Add change to last commit**

```text
git commit --amend
```

----

### Pushing commits

**Push to a new branch**

```text
git push -u origin HEAD
```

**Existing branch**

```text
git push
```

**Push and overwrite branch history**

```text
git push -f
```

---

## git Resources

----

### Flight rules for Git

Guide about what to do when things go wrong

https://github.com/k88hudson/git-flight-rules

----

### Awesome Git

A curated list of amazingly awesome Git tools, resources and shiny things.

https://github.com/dictcp/awesome-git

----

### Pro Git

Free Git book which includes an explanation of git internals.

https://git-scm.com/book/en/v2

---

## Questions?
